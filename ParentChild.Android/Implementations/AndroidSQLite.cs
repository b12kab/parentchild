﻿using System.IO;
using System.Diagnostics;
using ParentChild.Helpers;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(ParentChild.Droid.Implementations.AndroidSQLite))]

namespace ParentChild.Droid.Implementations
{
    public class AndroidSQLite : ISQLite
    {
        public AndroidSQLite()
        {
        }

        public SQLiteConnection GetConnection()
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            // Documents folder  
            var path = Path.Combine(documentsPath, DatabaseHelper.DbFileName);
            var conn = new SQLiteConnection(path);

            // Return the database connection  
            return conn;

        }
    }
}
