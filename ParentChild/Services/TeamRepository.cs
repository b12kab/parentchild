﻿using System;
using System.Collections.Generic;
using ParentChild.Helpers;
using ParentChild.Models;
using SQLite;

namespace ParentChild.Services
{
    public class TeamRepository : ITeamRepository
    {
        DatabaseHelper _databaseHelper;

        public TeamRepository(SQLiteConnection connection)
        {
            _databaseHelper = new DatabaseHelper(connection);
        }

        public void DeleteAllTeam()
        {
            _databaseHelper.DeleteAllTeam();
        }

        public void DeleteTeam(int id)
        {
            _databaseHelper.DeleteTeam(id);
        }

        public List<Team> GetAllTeamData()
        {
            return _databaseHelper.GetAllTeamData();
        }

        public Team GetTeamData(int id)
        {
            return _databaseHelper.GetTeamData(id);
        }

        public void InsertTeam(Team obj)
        {
            _databaseHelper.InsertTeam(obj);
        }

        public void UpdateTeam(Team obj)
        {
            _databaseHelper.UpdateTeam(obj);
        }

        public bool ValidateTeamName(string name)
        {
            var count = _databaseHelper.GetTeamNameCount(name);

            if (count > 0)
                return false;
            
            return true;
        }
    }
}
