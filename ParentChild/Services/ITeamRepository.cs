﻿using System;
using System.Collections.Generic;
using ParentChild.Models;

namespace ParentChild.Services
{
    public interface ITeamRepository
    {
        List<Team> GetAllTeamData();

        //Get Specific Contact data  
        Team GetTeamData(int id);

        // Delete all Excercise Data  
        void DeleteAllTeam();

        // Delete Specific Excercise  
        void DeleteTeam(int id);

        // Insert new Contact to DB   
        void InsertTeam(Team obj);

        // Update Contact Data  
        void UpdateTeam(Team obj);

        // Validate that Team is unique
        bool ValidateTeamName(string name);
    }
}
