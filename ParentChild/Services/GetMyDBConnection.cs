﻿using System;
using ParentChild.Helpers;
using SQLite;

namespace ParentChild.Services
{
    public class GetMyDBConnection
    {
        SQLiteConnection _sqliteconnection;
        public SQLiteConnection Connection
        {
            get
            {
                return _sqliteconnection;
            }
        }

        public GetMyDBConnection()
        {
            _sqliteconnection = Xamarin.Forms.DependencyService.Get<ISQLite>().GetConnection();
        }
    }
}
