﻿using System;
using System.Windows.Input;
using SQLite;

namespace ParentChild.Models
{
    [Table("Player")]
    public class Player
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        [Ignore]
        public ICommand TogglePlayerCommandWithParameterCommand { get; set; } 
        [Ignore]
        public bool Flag { get; set; }
    }
}
