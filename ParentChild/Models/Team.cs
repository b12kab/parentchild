﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;

namespace ParentChild.Models
{
    [Table("Team")]
    public class Team
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Blob { get; private set; }

        public List<int> GetPlayers()
        {
            List<int> returnList = new List<int>();

            if (!String.IsNullOrEmpty(this.Blob)) {
                string[] bits = Blob.Split(',');
                foreach (string bit in bits) {
                    try
                    {
                        int numVal = Convert.ToInt32(bit);
                        returnList.Add(numVal);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to convert player value to int: " + bit + 
                                          " error: " + e.Message);
                    }
                }
            }

            return returnList;
        }

        public void SetPlayers(List<int> list)
        {
            Blob = MakeBlob(list);
        }

        public static string MakeBlob(List<int> list)
        {
            string retVal = "";

            if (list.Any())
            {
                retVal = string.Join(",", list.Select(n => n.ToString()).ToArray());
            }

            return retVal;
        }

        public void ChangePlayers(bool flag, int id) 
        {
            List<int> currentPlayers = GetPlayers();
            var exists = currentPlayers.Contains(id);

            if (flag)
            {
                if (exists) {
                    return;
                } else {
                    currentPlayers.Add(id);
                    SetPlayers(currentPlayers);
                }
            } else {
                if (exists) {
                    currentPlayers.Remove(id);
                    SetPlayers(currentPlayers);
                }
            }
        }
    }
}
