﻿using System;
using SQLite;

namespace ParentChild.Helpers
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
