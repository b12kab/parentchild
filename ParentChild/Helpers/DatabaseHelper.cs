﻿using System;
using System.Collections.Generic;
using System.Linq;
using ParentChild.Models;
using SQLite;

namespace ParentChild.Helpers
{
    public class DatabaseHelper
    {
        static SQLiteConnection sqliteconnection;
        public const string DbFileName = "information.db3";

        public DatabaseHelper(SQLiteConnection connection)
        {
            sqliteconnection = connection;
            sqliteconnection.CreateTable<Player>();
            sqliteconnection.CreateTable<Team>();
        }

        //--------------------- Player -----------------------------------------
        public List<Player> GetAllPlayerData()
        {
            return (from data in sqliteconnection.Table<Player>()
                    select data).ToList();
        }

        public Player GetPlayerData(int id)
        {
            return sqliteconnection.Table<Player>().FirstOrDefault(t => t.Id == id);
        }

        public void DeleteAllPlayer()
        {
            sqliteconnection.DeleteAll<Player>();
        }

        public void DeletePlayer(int id)
        {
            sqliteconnection.Delete<Player>(id);
        }

        public void InsertPlayer(Player excercise)
        {
            sqliteconnection.Insert(excercise);
        }

        public void UpdateExcercise(Player player)
        {
            sqliteconnection.Update(player);
        }

        public int GetPlayerNameCount(string name)
        {
            return sqliteconnection.Table<Player>()
                                   .Where(x => x.Name == name)
                                   .Count();
        }

        //--------------------- Team -------------------------------------------
        public List<Team> GetAllTeamData()
        {
            return (from data in sqliteconnection.Table<Team>()
                    select data).ToList();
        }

        public void DeleteAllTeam()
        {
            sqliteconnection.DeleteAll<Team>();
        }

        public Team GetTeamData(int id)
        {
            return sqliteconnection.Table<Team>().FirstOrDefault(t => t.Id == id);
        }

        public void DeleteTeam(int id)
        {
            sqliteconnection.Delete<Team>(id);
        }

        public void InsertTeam(Team team)
        {
            sqliteconnection.Insert(team);
        }

        public void UpdateTeam(Team program)
        {
            sqliteconnection.Update(program);
        }

        public int GetTeamNameCount(string name)
        {
            return sqliteconnection.Table<Team>()
                                   .Where(x => x.Name == name)
                                   .Count();
        }
    }
}
