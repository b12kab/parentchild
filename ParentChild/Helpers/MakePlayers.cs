﻿using System;
using ParentChild.Models;
using SQLite;

namespace ParentChild.Helpers
{
    public class MakePlayers
    {
        public void PopulatePlayers(SQLiteConnection connection) {
            var helper = new DatabaseHelper(connection);

            helper.DeleteAllPlayer();

            var player1 = new Player();
            player1.Name = "Player one";
            player1.Position = "Left field";

            helper.InsertPlayer(player1);

            var player2 = new Player();
            player2.Name = "Player two";
            player2.Position = "Center field";

            helper.InsertPlayer(player2);

            var player3 = new Player();
            player3.Name = "Player three";
            player3.Position = "Right field";

            helper.InsertPlayer(player3);

            var player4 = new Player();
            player4.Name = "Player four";
            player4.Position = "First base";

            helper.InsertPlayer(player4);

            var player5 = new Player();
            player5.Name = "Player five";
            player5.Position = "Second base";

            helper.InsertPlayer(player5);

            var player6 = new Player();
            player6.Name = "Player six";
            player6.Position = "Shortstop";

            helper.InsertPlayer(player6);

            var player7 = new Player();
            player7.Name = "Player seven";
            player7.Position = "Third base";

            helper.InsertPlayer(player7);

            var player8 = new Player();
            player8.Name = "Player eight";
            player8.Position = "Catcher";

            helper.InsertPlayer(player8);

            var player9 = new Player();
            player9.Name = "Player nine";
            player9.Position = "Pitcher";

            helper.InsertPlayer(player9);

            var player10 = new Player();
            player10.Name = "Player ten";
            player10.Position = "Relief pitcher";

            helper.InsertPlayer(player10);

            var player11 = new Player();
            player11.Name = "Player eleven";
            player11.Position = "Misc relief";

            helper.InsertPlayer(player11);
        }
    }
}
