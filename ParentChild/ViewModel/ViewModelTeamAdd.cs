﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using ParentChild.Helpers;
using ParentChild.Models;
using ParentChild.Services;
using Xamarin.Forms;

namespace ParentChild.ViewModel
{
    public class ViewModelTeamAdd : ViewModelTeamBase
    {
        public ICommand AddTeamCommand { get; private set; }
        public int TogglePlayerWithParameterResult { get; private set; }

        public ViewModelTeamAdd(INavigation navigation)
        {
            _navigation = navigation;
            GetMyDBConnection connection = new GetMyDBConnection();
            _repository = new TeamRepository(connection.Connection);
            _table = new Team();

            DatabaseHelper dbHelper = new DatabaseHelper(connection.Connection);

            FetchData(dbHelper);
            AddTeamCommand = new Command(async () => await AddTeam());
        }

        private void FetchData(DatabaseHelper dbHelper)
        {
            // Yeah, this should be in the _Repository, but you know how it goes :)
            PlayerList = dbHelper.GetAllPlayerData();

            // Pointer out the trigger event here
            foreach (Player player in PlayerList)
            {
                player.TogglePlayerCommandWithParameterCommand = new Command<Player>(SetToggleSwitch);
            }
        }

        void SetToggleSwitch(Player player)
        {
            _table.ChangePlayers(player.Flag, player.Id);
            NotifyPropertyChanged("SetToggleSwitchPlayerAdd");
        }

        async Task AddTeam()
        {
            bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Add Team", "Do you want to save team details?", "OK", "Cancel");
            if (isUserAccept)
            {
                _repository.InsertTeam(_table); 
                await _navigation.PopAsync(true);
            }
        }
    }
}
