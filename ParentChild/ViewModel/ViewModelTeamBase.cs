﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ParentChild.Models;
using ParentChild.Services;
using Xamarin.Forms;

namespace ParentChild.ViewModel
{
    public class ViewModelTeamBase
    {
        public Team _table;

        public INavigation _navigation;
        public ITeamRepository _repository;

        public string Name
        {
            get => _table.Name;
            set
            {
                _table.Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public List<int> ChosenPlayers
        {
            get => _table.GetPlayers();
            set => _table.SetPlayers(value);
        }

        private List<Player> _playerList;
        public List<Player> PlayerList
        {
            get { return _playerList; }
            set
            {
                _playerList = value;
                NotifyPropertyChanged("PlayerList");
            }
        }

        private List<Team> _teams;
        public List<Team> Teams
        {
            get => _teams;
            set
            {
                if (value != null)
                {
                    _teams = value;
                    NotifyPropertyChanged("UpdatedTeamList");
                }
            }
        }

        //private Player _selectedPlayerItem;
        //public Player SelectedPlayer
        //{
        //    get { return _selectedPlayerItem; }
        //    set {
        //        _selectedPlayerItem = value;
        //        _
        //    }

        //}

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
