﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using ParentChild.Helpers;
using ParentChild.Models;
using ParentChild.Services;
using Xamarin.Forms;

namespace ParentChild.ViewModel
{
    public class ViewModelTeamDetail : ViewModelTeamBase
    {
        DatabaseHelper _dbHelper;

        public ICommand UpdateCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }
        public int TogglePlayerWithParameterResult { get; private set; }

        public ViewModelTeamDetail(INavigation navigation, int id)
        {
            _navigation = navigation;
            GetMyDBConnection connection = new GetMyDBConnection();
            _repository = new TeamRepository(connection.Connection);

            _dbHelper = new DatabaseHelper(connection.Connection);

            _table = new Team
            {
                Id = id
            };

            UpdateCommand = new Command(async () => await UpdateRow());
            DeleteCommand = new Command(async () => await DeleteRow());

            FetchData(_dbHelper);
        }

        void FetchData(DatabaseHelper dbHelper)
        {
            _table = _repository.GetTeamData(_table.Id);

            // Yeah, this should be in the _Repository, but you know how it goes :)
            PlayerList = dbHelper.GetAllPlayerData();

            // Pointer out the trigger event here
            foreach (Player player in PlayerList)
            {
                player.Flag = false;
                player.TogglePlayerCommandWithParameterCommand = new Command<Player>(SetToggleSwitch);
            }

            // Loop thru the list of player id's
            foreach (int playerIdFlag in ChosenPlayers)
            {
                // Loop thru the PlayerList to check for the playerIdFlag
                // If found, set the flag = true
                foreach (Player player in PlayerList)
                {
                    if (player.Id == playerIdFlag)
                        player.Flag = true;
                }
            }
        }

        void SetToggleSwitch(Player player)
        {
            _table.ChangePlayers(player.Flag, player.Id);
            NotifyPropertyChanged("SetToggleSwitchPlayerUpdate");
        }

        async Task UpdateRow()
        {
            bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Team Details", "Update this Team?", "OK", "Cancel");
            if (isUserAccept)
            {
                _repository.UpdateTeam(_table);
                await _navigation.PopAsync();
            }
        }

        async Task DeleteRow()
        {
            bool isUserAccept = await Application.Current.MainPage.DisplayAlert("Team Details", "Delete this Team?", "OK", "Cancel");
            if (isUserAccept)
            {
                _repository.DeleteTeam(_table.Id);
                await _navigation.PopAsync();
            }
        }
    }
}
