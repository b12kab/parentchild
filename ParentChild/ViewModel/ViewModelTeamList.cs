﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using ParentChild.Helpers;
using ParentChild.Models;
using ParentChild.Services;
using ParentChild.Views;
using Xamarin.Forms;

namespace ParentChild.ViewModel
{
    public class ViewModelTeamList : ViewModelTeamBase
    {
        public ICommand AddCommand { get; private set; }

        public ViewModelTeamList(INavigation navigation)
        {
            _navigation = navigation;
            GetMyDBConnection connection = new GetMyDBConnection();
            _repository = new TeamRepository(connection.Connection);

            AddCommand = new Command(async () => await ShowAddPage());

            FetchData();
        }

        private void FetchData() {
            Teams = _repository.GetAllTeamData();
        }

        public void UpdateData() {
            FetchData();
        }

        async Task ShowAddPage()
        {
            await _navigation.PushAsync(new ViewTeamAdd());
        }

        async void ShowDetailPage(int id)
        {
            await _navigation.PushAsync(new ViewTeamDetail(id));
        }

        Team _selectedTeam;
        public Team SelectedTeamItem
        {
            get => _selectedTeam;
            set
            {
                if (value != null)
                {
                    _selectedTeam = value;
                    NotifyPropertyChanged("SelectedTeamItem");
                    ShowDetailPage(value.Id);
                }
            }
        }
    }
}
