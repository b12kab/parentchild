﻿using System;
using System.Collections.Generic;
using ParentChild.ViewModel;
using Xamarin.Forms;

namespace ParentChild.Views
{
    public partial class ViewTeamDetail : ContentPage
    {
        public ViewTeamDetail(int id)
        {
            InitializeComponent();
            this.BindingContext = new ViewModelTeamDetail(Navigation, id);  
        }
    }
}
