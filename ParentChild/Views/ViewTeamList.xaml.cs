﻿using System;
using System.Collections.Generic;
using ParentChild.ViewModel;
using Xamarin.Forms;

namespace ParentChild.Views
{
    public partial class ViewTeamList : ContentPage
    {
        public ViewTeamList()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            BindingContext = new ViewModelTeamList(Navigation);
        }
    }
}
