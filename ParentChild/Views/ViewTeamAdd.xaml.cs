﻿using ParentChild.ViewModel;
using Xamarin.Forms;

namespace ParentChild.Views
{
    public partial class ViewTeamAdd : ContentPage
    {
        public ViewTeamAdd()
        {
            InitializeComponent();
            //BindingContext = new ViewModelTeamAdd(Navigation);
        }

        protected override void OnAppearing()
        {
            BindingContext = new ViewModelTeamAdd(Navigation);
        }
    }
}
