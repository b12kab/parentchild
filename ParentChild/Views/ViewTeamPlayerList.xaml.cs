﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ParentChild.Views
{
    public partial class ViewTeamPlayerList : ContentView
    {
        public ViewTeamPlayerList()
        {
            InitializeComponent();

            MyList.ItemSelected += (object sender, SelectedItemChangedEventArgs e) => {
                ((ListView)sender).SelectedItem = null;
            };
        }
    }
}
