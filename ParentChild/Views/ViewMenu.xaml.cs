﻿using System;
using System.Collections.Generic;
using ParentChild.Helpers;
using ParentChild.Services;
using Xamarin.Forms;

namespace ParentChild.Views
{
    public partial class ViewMenu : ContentPage
    {
        public ViewMenu()
        {
            InitializeComponent();
        }

        void AddPlayers_Clicked(object sender, System.EventArgs e)
        {
            MakePlayers make = new MakePlayers();
            GetMyDBConnection connection = new GetMyDBConnection();
            make.PopulatePlayers(connection.Connection);
            DisplayAlert("Add Players", "Players added", "OK");
        }

        async void AddTeam_Clicked(object sender, System.EventArgs e)
        {
            //MainPage = new NavigationPage(new ViewTeamAdd());
            await Navigation.PushAsync(new ViewTeamAdd());
        }

        async void ViewTeam_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ViewTeamList());
            //NavigationPage(new ViewTeamList());
        }
    }
}
