﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParentChild.Helpers;
using ParentChild.Services;
using ParentChild.Views;
using Xamarin.Forms;

namespace ParentChild
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void AddPlayers_Clicked(object sender, System.EventArgs e)
        {
            MakePlayers make = new MakePlayers();
            GetMyDBConnection connection = new GetMyDBConnection();
            make.PopulatePlayers(connection.Connection);
            DisplayAlert("Add Players", "Players added", "OK");
        }

        void AddTeam_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new ViewTeamAdd());
        }

        void ViewTeam_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new ViewTeamList());
            //NavigationPage(new ViewTeamList());
        }
    }
}
