﻿using System;
using System.IO;
using ParentChild;
using ParentChild.Helpers;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(ParentChild.iOS.Implementations.IOSSQLite))]
namespace ParentChild.iOS.Implementations
{
    public class IOSSQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder  
            string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder  
            var path = Path.Combine(libraryPath, DatabaseHelper.DbFileName);
            // Create the connection  
            //var plat = new SQLite.Platform.XamarinIOS.SQLitePlatformIOS();
            //var conn = new SQLiteConnection(plat, path);
            var conn = new SQLiteConnection(path);

            // Return the database connection  
            return conn;
        }
    }
}
