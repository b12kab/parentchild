﻿using System;
using System.Collections.Generic;
using System.IO;
using ParentChild.Helpers;
using ParentChild.Models;
using SQLite;
using Xunit;

namespace XunitTests
{
    public class CreatePlayer
    {
        [Fact]
        public void CreateTestPlayers()
        {
            var connection = GetConnection();

            MakePlayers makePlayers = new MakePlayers();
            makePlayers.PopulatePlayers(connection);
            Assert.True(true);
        }

        public SQLiteConnection GetConnection()
        {
            string thisPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            var path = Path.Combine(documentsPath, DatabaseHelper.DbFileName);

            return new SQLiteConnection(path);
        }
    }
}
